# DSV - CSS

## Getting started:
### Content:
* [Images](http://unsplash.it/ "Unsplash")
* [Placeholder/Text Block](http://meettheipsums.com/)
* [Stock Photos](https://publicdomainarchive.com "100% Free Stock Photos")
### Naming conventions
* use lowercase
* using dash to separate words
* avoid linking directly to file in other websites (third-party used to specifically hosting is fine)

## CSS core
### Terminology and Syntax:
* **Selectors**: determine HTML ele
* **Declaration blocks**: consist of style rule(s), enclosed in {}
* **Declaration**: style rule
* **Properties**: type of style
* **Value**: properties of style
* ### Resources:
	* [codrops](https://tympanus.net/codrops/)
	> reference: [codrops](https://tympanus.net/codrops/css_reference/)
	* Mozilla Development Network
* **Pseudo-class**: hover, link, visited, active, focus...
* **Descendant selectors**: avoid going more than 3 lvls deep
* **Combining selectors**: each selector is independent of each other. To target both selectors, combine them w/o a sapce
* ### Color resources:
	* [CCS colours name](http://colours.neilorangepeel.com/ "list all available keywords, hex, RGB")
	* [Coolors](https://coolors.co/ "generate color pallette")
	* [Random A11y](https://randoma11y.com "color combination")
* ### Cascading, inheritance, and specificity:
	* [Table of properties](https://www.w3.org/TR/CSS21/propidx.html "inheritance of properties")
	* Specificity ranking: ID > class > type
	* [Comparing ranking](http://specificity.keegan.st/)

## Typography:
* **Typography**: the study of the design and use of type for communication
* **Typeface**: a set of fonts, designed w/ common characteristics and composed of glyphs
* **Font**: invidual files that are part of a typeface
	> Serif: has decorative line <br/>
Sans-Serif: has no line <br/>
Monospace: same width, code font
* ### Web fonts and google font
	* **internal**: `@font-face`: used to set the font name and link to the font files
		* refer to code.01
	* **external**: `@import url(link)`

		* [google font](https://fonts.google.com/ "free")
		* [typekit](https://typekit.com/fonts "paid")
		* [more info](https://goo.gl/rZZEEe)
* ### The font-size property:
	*  **pixel (px)**: absolute value, default: 16px. Avoid using decimal.
	* **em**: relative (1em = inherited font size). Default = 16px
	* **rem**: similar to em, but only relative to root ele (<html>)
* ### Font-style and font-weight properties:
	* `font-weight`: [100,900]
		> normal=400, bold=700
	* `font-style`: italic, oblique, normal
* ### Color, line-height, text properties
	* `line-height`: set height of space between 2 lines of text, related to `font-size`.
	* `text-decoration`: overline, underline, line-throigh, none
	* `text-transform`: capitalize, upper/lowercase, none
	* `text-align`


## Layouts
* [Display ref.](https://goo.gl/ma6VqT) <br/>
	Inline element doesnt have width or height. Block element does.
* ### The box model
	* *content*: has w and h
	* `padding`: space inside of the ele
	* `border`: [reference](https://goo.gl/MUfrwP "MDN")
	* `margin`: space outside (inline ele only has margin horizontally)
	* using `max-width` makes page more flexible
* ### Floats
	* dont change element's natural flow, but remove it from the page's natural flow
	* `clear`
	* float elements cant be recognized by parent elements
	* self-clearing:
		* `overflow`
		* clearfix hack: refer to code.02
		* [more](https://css-tricks.com/snippets/css/clear-fix "Force Element to Self-clear its Children") info about self-clearing
		* [Check the browser support for CSS properties](https://caniuse.com "Can I use...?")
		* `border-radius: 50%` : change square img to round img
* **Box model fix**: changing box-sizing to border-box, hence including border and padding in calculating total size. Reference [here](https://goo.gl/huQz3L)
* using `min-height` will allow the content to flow down even after the specific height is reached.
* **Positioning**
	* Position is used to arrange elements relative to the default page flow or browser viewport. Position is also used with a combination of offset properties: `top`,` right`, `bottom`, `left`.
	* Five values:  
		* `static`: default, not position
		* `inherit`: inherits value from ancestor
		* `relative`: stay in nature flow, but can use offset to align (rmb to put in content element if u plan on using absolute)
		* `absolute`: remove from nature page flow
		* `fixed`: stay in one position, even when page scrolll
	* Reference [here](https://goo.gl/xJsybu "CoDrops")
* **Float, display or position?**
	* `float`: for flexible content (img surrounded by text), or for large/global part of page
	* `display`: align page components (make sure to count extra space), elements need to be center align, and it doesnt change page natural flow
	* `poition`: element need to positioned relative to another element, outside of  natural flow, to a specific spot (shouldnt use for page layout)
* **Layer**
	* normal element < float < inline < position
	* (must have position)`z-index`: choose your own stacking order. But if we can rearrange HTML code to change the stacking order, do it instead.

## CSS selectors
* using [] to select type element: `[type="input"]{}`
* **Combinator selectors**:
	* use `>` (ex: `section > a`) to select the child element only
	* Sibling:
		* Adjacent `+`: the first sibling following the first selector
		* Tilde `~`: select any sibling following
	* without space, only select the element has both class/id
* **Pseudo-class selector**
	* `:first-child` and `:last-child`: count the element not the target type as well
	* `:first-of-type` and `:last-of-type`
	* `:nth-child(keyword/number/algebraic expression)`: used to select one or more child element based on the order within the parent container
		* keyword: `odd` and `even`
		* an+b: n start w/ zero, increse by one, a is the step, b is the start
	* `:nth-of-type`
	* more info [here](https://www.w3schools.com/css/css_pseudo_classes.asp)
* **Pseudo-element selector**
	* `:before` and `:after`: used to generate elements that are inserted before or after selected element
	* [Unicode character table](https://unicode-table.com)
	* more info [here](https://www.w3schools.com/css/css_pseudo_elements.asp)

## Tips and tools
* **Icon fonts**
	> Add imagery to your website w/o using images, can be styled
	* [Font Awesome](https://goo.gl/BT3EsI)
* **The background property**
	* `background-image`: when you want to use an img as your bg
	* `background-repeat`
	* `background-position`
	* `background-attachment`: be careful when using this with width and height
	* `background-size`: cover (safer option, but also not perfect), should be separated in shorthand syntax, when using in shorthand, a `background-position` value must also be declared
* **Alpha transparency and gradients**
	* `rgba(r, g, b, a)` with a = [0, 1], 0 = transparent, 1 = opaque
	* require 2 cover, 1 for img, 1 for the transparent layer
	* multiple img can be added together, separated by comma
	* gradient: `linear-gradient(color, color)`
	* ex: refer to code.03

## Responsive and mobile
* ## Introduction
	* [Media Queries](http://mediaqueri.es "Collection of responsive designs")
	* [liquidapsive](http://liquidapsive.com "Example and info about differnt styles")
	* **Breakpoints**: the requirements used to determine when to change the styles and layout of your webpage, to optimize for mobile, tablet,...
* ## Mobile First
	Focusing on small screens first may help you make more thoughtful decisions about what element is necessary
	* *Grateful Degradation*: modern browsers first, provide fallbacks,..
	* *Progressive Enhancement*: base-lvl experience first, focus on content and accessibility, than add more feature for modern browsers later (opposite of GD). More thoughtful for mobile.
* ## Creating flexible and fluid layouts
	* Start w/ fluid CSS before responsive
	* Make the content fit relative to its owner
	* Dont rely on defined devices size (cuz there's *many* devices gonna view your web)
	* Use `percentage-based` widths for page components (it's *really* good)
	* Use `min-` and `max` (*flexibility*)
* ## Media Queries
	* **Media Queries**: Create conditions for applying specific CSS styles. If a condition is met, apply this CSS. If not, dont.
	* **Media Types**
		* `print`
		* `speech`: screen readers
		* `screen`: not 2 above
	* **Media Feature**:
		* same syntax w/ CSS properties
		* describe the requirement of a device to apply the styles to, or a single feature of the device (w, h, orientation,..)
		* [CoDrops reference](https://goo.gl/1sF9zC "Media Queries")
		* MF - Width: use `min-` or `max-`, combine them to form a range
		* **Adding MQ**
			* using the `<link>` tag in *HTML* file: refer to code.04
			* using `@media` in *CSS* file: refer to code.05
			* [MDN reference](https://goo.gl/cVHo8U "Using media queries")
		* try not to use so many MQ (due to unnecessary CSS), try to combine them
		* `<meta name="viewport">`(viewport meta tag) help MQ to work properly on mobile devices.
		* default, MQ applies to all feature in page.

### Reference Code:
#### Code.01
```css
@font-face{
	font-family: Arial;
	src: url(link) format('woof');
}
```
#### Code.02
```css
.clearfix:after{
	content: ""
	display: table
	clear: both
}
```
#### Code.03
```css
selector{
	background: linear-gradient(rgba(255, 255, 255, 0.8),
			rgba(199, 21, 133, 0.5)),
		url(image.png) no-repeat
}
```
#### Code.04
```html
<link media="screen and (mad-width: 400px)" rel="stylesheet" href="mobile.css">
```
#### Code.05
```css
@media (mad-width: 400px){
	.selector{
		/*write something here*/
	}
}
```
